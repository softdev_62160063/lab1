/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.kob.lab1;

/**
 *
 * @author Murphy
 */
import java.util.Arrays;
import java.util.Scanner;
public class Lab1 {
    
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Character[] Xo_Check = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'};
        
        Character Turn = 'O';
        
        int row = 0;
        int col = 0;
        
        System.out.println("Welcome to OX Game");
        System.out.println("|-| |-| |-|");
        System.out.println("|-| |-| |-|");
        System.out.println("|-| |-| |-|");
        System.out.println("-----------------------------");
        
        
        System.out.println("Player O Play First");
        
        Boolean CheckWin = false;
        String TurnOX = "null";
                
        for (int i = 0; i <9; i++) {
            
            if (CheckWin == true) {
                //System.out.println(CheckWin);//
                break;
            }
            System.out.println("Round : " + (i+1));
            System.out.print("Turn : ");
            if (i % 2 == 0) {
                TurnOX = "O";
                System.out.println("O");
            } else {
                TurnOX = "X";
                System.out.println("X");
            }
            System.out.print("Please input row, col: ");
            row = kb.nextInt();
            col = kb.nextInt();


            for (boolean loop_check = false; loop_check == false;) {
                if ((row == 1 || row == 2 || row == 3) && (col == 1 || col == 2 || col == 3)) {
                    loop_check = true;
            } else {
                    System.out.println("Wrong Number, Please Try Again!!!");
                    System.out.print("Please input row, col: ");
                    row = kb.nextInt();
                    col = kb.nextInt();
                }
            }
            
             if (i % 2 == 0) {
                if (row == 1 && col == 1) {
                    Xo_Check[0] = 'O';
                } else {
                    if (row == 1 && col == 2 ) {
                        Xo_Check[1] = 'O';
                    } else {
                        if (row == 1 && col == 3) {
                            Xo_Check[2] = 'O';
                        } else {
                            if (row == 2 && col == 1 ){
                                Xo_Check[3] = 'O';
                            } else {
                                if (row == 2 && col == 2) {
                                    Xo_Check[4] = 'O';
                                } else {
                                    if (row == 2 && col == 3) {
                                        Xo_Check[5] = 'O';
                                    } else {
                                        if (row == 3 && col == 1) {
                                           Xo_Check[6] = 'O';
                                        } else {
                                            if (row == 3 && col == 2) {
                                                Xo_Check[7] = 'O';
                                            } else {
                                                Xo_Check[8] = 'O';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (row == 1 && col == 1) {
                    Xo_Check[0] = 'X';
                } else {
                    if (row == 1 && col == 2 ) {
                        Xo_Check[1] = 'X';
                    } else {
                        if (row == 1 && col == 3) {
                            Xo_Check[2] = 'X';
                        } else {
                            if (row == 2 && col == 1 ){
                                Xo_Check[3] = 'X';
                            } else {
                                if (row == 2 && col == 2) {
                                    Xo_Check[4] = 'X';
                                } else {
                                    if (row == 2 && col == 3) {
                                        Xo_Check[5] = 'X';
                                    } else {
                                        if (row == 3 && col == 1) {
                                           Xo_Check[6] = 'X';
                                        } else {
                                            if (row == 3 && col == 2) {
                                                Xo_Check[7] = 'X';
                                            } else {
                                                Xo_Check[8] = 'X';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }



            System.out.println("|" + Xo_Check[0].toString() + "|" + " " + "|" + Xo_Check[1].toString() + "|" + " " + "|" + Xo_Check[2].toString() + "|");
            System.out.println("|" + Xo_Check[3].toString() + "|" + " " + "|" + Xo_Check[4].toString() + "|" + " " + "|" + Xo_Check[5].toString() + "|");
            System.out.println("|" + Xo_Check[6].toString() + "|" + " " + "|" + Xo_Check[7].toString() + "|" + " " + "|" + Xo_Check[8].toString() + "|");
            System.out.println("");
            if (i >= 4){
                   if (Xo_Check[0] == Xo_Check[1] && Xo_Check[1] == Xo_Check[2]){
                       System.out.println("Player " + TurnOX + " Wins!");
                       CheckWin = true;
                   } else {
                       if (Xo_Check[3] == Xo_Check[4] && Xo_Check[4] == Xo_Check[5]) {
                           System.out.println("Player " + TurnOX + " Wins!");
                           CheckWin = true;
                       } else {
                           if (Xo_Check[6] == Xo_Check[7] && Xo_Check[7] == Xo_Check[8]) {
                                System.out.println("Player " + TurnOX + " Wins!");
                                CheckWin = true;
                           } else {
                               if (Xo_Check[0] == Xo_Check[3] && Xo_Check[3] == Xo_Check[6]) {
                                   System.out.println("Player " + TurnOX + " Wins!");
                                   CheckWin = true;
                               } else {
                                   if (Xo_Check[1] == Xo_Check[4] && Xo_Check[4] == Xo_Check[7]) {
                                       System.out.println("Player " + TurnOX + " Wins!");
                                       CheckWin = true;
                                   } else {
                                       if (Xo_Check[2] == Xo_Check[5] && Xo_Check[5] == Xo_Check[8]) {
                                           System.out.println("Player " + TurnOX + " Wins!");
                                           CheckWin = true;
                                       } else {
                                           if (Xo_Check[0] == Xo_Check[4] && Xo_Check[4] == Xo_Check[8]) {
                                               System.out.println("Player " + TurnOX + " Wins!");
                                               CheckWin = true;
                                           } else {
                                               if (Xo_Check[2] == Xo_Check[4] && Xo_Check[4] == Xo_Check[6]) {
                                                   System.out.println("Player " + TurnOX + " Wins!");
                                                   CheckWin = true;
                                               } else {
                                                   
                                               }
                                           }
                                       }
                                   }
                               }
                           }
                       }
                   }
            }
        }
        

        
        
    }
    
    
}
